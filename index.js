const express = require('express');
const http = require('http');
const WebSocket = require('ws');

const app = express();

app.use('/static', express.static('public'));
app.set('views', './views')
app.set('view engine', 'pug');

app.get('/', function (req, res) {
  res.render('index', { title: 'Fanfare', message: 'Fanfare multiplayer' })
})

const server = http.createServer(app);
const wss = new WebSocket.Server({ server, path: "/socket" });

let currentlyPlaying = { 1: true, 2: true, 3: true, 4: true, 5: true, 6: true };

wss.on('connection', function connection(ws, req) {
  console.log("[client connected]");
  wss.broadcast(ws, JSON.stringify({ meta: { usercount: wss.clients.size }}));

  ws.on('message', function incoming(message) {
    console.log('received: %s', message);

    try {

      const msg = JSON.parse(message);
      if (msg.update) {
        
        console.log('update came in: %s', msg);
        new_playing = currentlyPlaying[msg.update.toggled];
        if (new_playing === true) {
          currentlyPlaying[msg.update.toggled] = false;
        } else {
          currentlyPlaying[msg.update.toggled] = true;
        }
        wss.broadcast(ws, JSON.stringify({ update: { currentlyPlaying }, meta: { usercount: wss.clients.size }}));
      }

    } catch(e) {
      console.log('[error]', e)
    }
  });

  ws.on('disconnect', function(){
    console.log('[client disconnected]');
  });

});

wss.broadcast = function broadcast(sender, data) {
  console.log("[broadcasting]", data)
  wss.clients.forEach(function each(client) {
    client.send(data);
  });
};

totalUsers = function() {
  return wss.clients.size;
}

myPulse = setInterval(function() {
  wss.broadcast('server', JSON.stringify({ pulse: { currentlyPlaying }, meta: { usercount: totalUsers() } }) )
}, 5407);

server.listen(8080, function listening() {
  console.log('Listening on %d', server.address().port);
});