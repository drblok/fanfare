ws = new WebSocket('ws://'+window.location.host+'/socket');

let isPlaying = false;

ws.onopen = function(event) {
  console.log('connected, yay!');
};

ws.onmessage = function(event) {
  try {
    const msg = JSON.parse(event.data);
    // on the fly update (only mute/unmute)
    if (msg.update) {
      console.log('found update:', msg.update.currentlyPlaying);

      for (var key in msg.update.currentlyPlaying) {
        document.getElementById('audio-'+key).muted = !(msg.update.currentlyPlaying[key]);
        let indicatorPlaying = msg.update.currentlyPlaying[key];
        if (indicatorPlaying === true) {
          document.getElementById('indicator-'+key).style.color = "green";
        } else {
          document.getElementById('indicator-'+key).style.color = "red";
        }
        document.getElementById('indicator-'+key).innerHTML = indicatorPlaying;
      }
    }
    // pulse sent by server
    if (msg.pulse) {
      // console.log('found pulse:', msg.pulse.currentlyPlaying);
      
      // if audio isn't playing, start playing
      for (var key in msg.pulse.currentlyPlaying) {
        document.getElementById('audio-'+key).muted = !(msg.pulse.currentlyPlaying[key]);
        let indicatorPlaying = msg.pulse.currentlyPlaying[key];
        if (indicatorPlaying === true) {
          document.getElementById('indicator-'+key).style.color = "green";
        } else {
          document.getElementById('indicator-'+key).style.color = "red";
        }
        document.getElementById('indicator-'+key).innerHTML = indicatorPlaying;
      }

      [0,1,2,3,4,5].forEach(function(item) {
        document.getElementsByTagName("audio")[item].pause();
        document.getElementsByTagName("audio")[item].currentTime = 0;
        document.getElementsByTagName("audio")[item].play();
      });
      
    }

    // set current amount of users
    if (msg.meta) {
      if (msg.meta.usercount === 1) {
        document.getElementById('usercount').innerHTML = msg.meta.usercount + " composer";
      } else {
        document.getElementById('usercount').innerHTML = msg.meta.usercount + " composers";
      }
    }
    

  } catch(e) {
    console.log('[error]', e)
  }
};

// send toggle instrument to server
toggleInstrument = function(e) {
  ws.send(JSON.stringify({"update":{"toggled": e }}));
};