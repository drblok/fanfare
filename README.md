# Fanfare multiplayer

A multiplayer version of fanfare @ https://www.geluidsbord.nl/fanfare.

## Install

`npm install`

## Start

`node index.js`

Works when running on nginx + passenger.